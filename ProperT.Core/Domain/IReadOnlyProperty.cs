﻿namespace ProperT.Core.Domain;

public interface IReadOnlyProperty
{
    public Guid Id { get; }
    public string Address { get; }
}

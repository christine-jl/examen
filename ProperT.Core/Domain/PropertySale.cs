using ProperT.Core.Domain.Common;
using System.Diagnostics.CodeAnalysis;

namespace ProperT.Core.Domain;

public class PropertySale : IReadOnlyPropertySale
{
    public Guid Id { get; }
    public Identity Seller { get; }
    IReadOnlyIdentity IReadOnlyPropertySale.Seller => Seller;

    public PropertySale(Identity seller)
    : this(Guid.NewGuid(), seller)
    { }

    public PropertySale(Guid id, Identity seller)
    {
        Id = id;
        Seller = seller;
    }

    public PropertySaleStatus Status { get; private set; } = PropertySaleStatus.Draft;

    private Property? property;

    [DisallowNull] // Warns when trying to call the setter with a nullable value

    public Property? Property
    {
        get => property;
        set
        {
            if (Status != PropertySaleStatus.Draft)
                throw new InvalidOperationException($"Can't change property of sale {Id} because status is {Status} instead of Draft");

            property = value;
        }
    }
    IReadOnlyProperty? IReadOnlyPropertySale.Property => Property;

    private Identity? notary;

    [DisallowNull] // Warns when trying to call the setter with a nullable value
    public Identity? Notary
    {
        get => notary;
        set
        {
            if (Status != PropertySaleStatus.Draft)
                throw new InvalidOperationException($"Can't change notary of sale {Id} because status is {Status} instead of Draft");
            notary = value;
        }
    }
    IReadOnlyIdentity? IReadOnlyPropertySale.Notary => Notary;

    private Identity? buyer;

    [DisallowNull] // Warns when trying to call the setter with a nullable value
    public Identity? Buyer
    {
        get => buyer;
        set
        {
            if (Status != PropertySaleStatus.Submitted)
                throw new InvalidOperationException($"Can't change buyer of sale {Id} because status is {Status} instead of Submitted");
            buyer = value;
        }
    }
    IReadOnlyIdentity? IReadOnlyPropertySale.Buyer => Buyer;

    private Price? price;

    [DisallowNull] // Warns when trying to call the setter with a nullable value
    public Price? Price
    {
        get => price;
        set
        {
            if (Status != PropertySaleStatus.Draft)
                throw new InvalidOperationException($"Can't change price of sale {Id} because status is {Status} instead of Draft");
            price = value;
        }
    }

    public decimal SecurityDeposit => (price?.Amount ?? 0m) * 0.1m;
    public decimal AmountDue { get; private set; }

    [DisallowNull] // Warns when trying to call the setter with a nullable value
    public DateTime? AgreementDate { get; private set; }

    [DisallowNull] // Warns when trying to call the setter with a nullable value
    public DateTime? RecordingDate { get; private set; }

    [DisallowNull] // Warns when trying to call the setter with a nullable value
    public DateTime? SigningDate { get; private set; }

    private LoanRequirements? loanRequirements;
    public LoanRequirements? LoanRequirements
    {
        get => loanRequirements;
        set
        {
            if (Status != PropertySaleStatus.Submitted)
                throw new InvalidOperationException($"Can't change loan requirements of sale {Id} because status is {Status} instead of Submitted");
            loanRequirements = value;
        }
    }

    public void Submit()
    {
        if (Status != PropertySaleStatus.Draft)
            throw new InvalidOperationException($"Can't submit sale {Id} because status is {Status} instead of Draft");

        if (price == null)
            throw new InvalidOperationException($"Can't submit sale {Id} because no price has been set");

        if (Property == null)
            throw new InvalidOperationException($"Can't submit sale {Id} because no property has been set");

        if (Notary == null)
            throw new InvalidOperationException($"Can't submit sale {Id} because no notary has been set");

        Status = PropertySaleStatus.Submitted;
        AmountDue = SecurityDeposit;
    }

    public void RecordAgreement(DateTime agreementDate)
    {
        if (Status != PropertySaleStatus.Submitted)
            throw new InvalidOperationException($"Can't record agreement for sale {Id} because status is {Status} instead of Submitted");

        Status = PropertySaleStatus.Agreed;
        AgreementDate = agreementDate;
    }

    public void Cancel(DateTime cancellationDate)
    {
        if (Status != PropertySaleStatus.Agreed)
            throw new InvalidOperationException($"Can't cancel sale {Id} because status is {Status} instead of Agreed");

        Status = PropertySaleStatus.Cancelled;
        if (cancellationDate - AgreementDate <= TimeSpan.FromDays(31))
            AmountDue = 0m;
    }

    public void RecordSignature(DateTime signingDate)
    {
        if (Status != PropertySaleStatus.Agreed)
            throw new InvalidOperationException($"Can't record signature agreement for sale {Id} because status is invalid");

        Status = PropertySaleStatus.Signed;
        SigningDate = signingDate;
        AmountDue = price!.Amount; // Price cannot be null in this status
    }

    public void AcknowledgePublicRecording(DateTime recordingDate)
    {
        if (Status != PropertySaleStatus.Signed)
            throw new InvalidOperationException($"Can't acknowledge public recording for sale {Id} because status is {Status} instead of Signed");

        Status = PropertySaleStatus.Recorded;
        RecordingDate = recordingDate;
    }
}

// Question 14
/* 
public class PropertySale : IReadOnlyPropertySale
{
    public Guid Id { get; }
    public Identity Seller { get; }
    IReadOnlyIdentity IReadOnlyPropertySale.Seller => Seller;

    public PropertySale(Identity seller)
        : this(Guid.NewGuid(), seller)
    { }

    public PropertySale(Guid id, Identity seller)
    {
        Id = id;
        Seller = seller;
    }

    public PropertySaleStatus Status { get; private set; } = PropertySaleStatus.Draft;

    private Property? property;
    private Identity? notary;
    private Identity? buyer;
    private Price? price;
    private LoanRequirements? loanRequirements;

    public void SetProperty(Property? property) => SetWithStatusCheck(ref this.property, property, PropertySaleStatus.Draft);
    public void SetNotary(Identity? notary) => SetWithStatusCheck(ref this.notary, notary, PropertySaleStatus.Draft);
    public void SetBuyer(Identity? buyer) => SetWithStatusCheck(ref this.buyer, buyer, PropertySaleStatus.Submitted);
    public void SetPrice(Price? price) => SetWithStatusCheck(ref this.price, price, PropertySaleStatus.Draft);
    public void SetLoanRequirements(LoanRequirements? loanRequirements) => SetWithStatusCheck(ref this.loanRequirements, loanRequirements, PropertySaleStatus.Submitted);

    public void Submit() => PerformActionWithStatusCheck(PropertySaleStatus.Draft, () =>
    {
        // Additional checks before submitting
        if (price == null || Property == null || Notary == null)
            throw new InvalidOperationException("Cannot submit sale with incomplete details.");

        Status = PropertySaleStatus.Submitted;
        AmountDue = SecurityDeposit;
    });

    public void RecordAgreement(DateTime agreementDate) => PerformActionWithStatusCheck(PropertySaleStatus.Submitted, () =>
    {
        Status = PropertySaleStatus.Agreed;
        AgreementDate = agreementDate;
    });


    private void SetWithStatusCheck<T>(ref T field, T value, PropertySaleStatus requiredStatus)
    {
        if (Status != requiredStatus)
            throw new InvalidOperationException($"Can't change property of sale {Id} because status is {Status} instead of {requiredStatus}");

        field = value;
    }

    private void PerformActionWithStatusCheck(PropertySaleStatus requiredStatus, Action action)
    {
        if (Status != requiredStatus)
            throw new InvalidOperationException($"Cannot perform action because status is {Status} instead of {requiredStatus}");

        action();
    }

}
*/

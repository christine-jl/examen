﻿namespace ProperT.Core.Domain;

public record LoanRequirements(decimal Amount, decimal Rate, int DurationInMonths);

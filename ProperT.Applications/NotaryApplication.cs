﻿using ProperT.Core.Repositories;

namespace ProperT.Core.Applications;

public class NotaryApplication
{
    private readonly IPropertyRepository propertyRepository;
    private readonly IPropertySaleRepository propertySaleRepository;
    private readonly IOwnershipGatewayService ownershipGatewayService;

    public NotaryApplication(
        IPropertyRepository propertyRepository,
        IPropertySaleRepository propertySaleRepository,
        IOwnershipGatewayService ownershipGatewayService)
    {
        this.propertyRepository = propertyRepository;
        this.propertySaleRepository = propertySaleRepository;
        this.ownershipGatewayService = ownershipGatewayService;
    }

    public async Task<IReadOnlyPropertySale?> GetPropertySale(Guid currentUserId, Guid saleId)
    {
        return await GetPropertySaleWithAccessRights(currentUserId, saleId);
    }

    private async Task<PropertySale?> GetPropertySaleWithAccessRights(Guid currentUserId, Guid saleId)
    {
        var sale = await propertySaleRepository.GetFirstOrDefault();
        if (sale?.Notary?.Id != currentUserId)
            return null;

        return sale;
    }

    public async Task<IReadOnlyPropertySale> SignPropertySale(IReadOnlyPropertySale sale)
    {
        var mutableSale = (PropertySale)sale;

        await ownershipGatewayService.EnsurePropertyIsOwnedBySeller(mutableSale, ensureNotTransferring: false);

        mutableSale.RecordSignature(DateTime.Now);

        await ownershipGatewayService.PublishStatusUpdate(mutableSale);
        await propertySaleRepository.Save(mutableSale);

        return mutableSale;
    }

    public async Task<IReadOnlyPropertySale> AcknoledgePublicRecording(IReadOnlyPropertySale sale)
    {
        var mutableSale = (PropertySale)sale;
        mutableSale.AcknowledgePublicRecording(DateTime.Now);
        await propertySaleRepository.Save(mutableSale);

        return mutableSale;
    }

    public async Task<IEnumerable<string>> FindListOfOwnersForProperty(string s)
    {
        List<string> s1 = new List<string>();
        var p = await propertyRepository.Find(s);
        if (p.Id != null)
        {
            List<PropertySale> s2 = (await propertySaleRepository.FindForProperty(p.Id)).ToList();
            s2.Sort((s3, s4) => Nullable.Compare(s3.SigningDate, s4.SigningDate) * -1);
            // Tri descendant par SigningDate
            foreach (var s5 in s2)
            {
                if (s5.Status >= PropertySaleStatus.Signed && s5.Status != PropertySaleStatus.Cancelled)
                {
                    if (s1.Count == 0 || s1[s1.Count - 1] != s5.Buyer.FullName)
                        s1.Add(s5.Buyer.FullName);
                    if (s1.Count == 0 || s1[s1.Count - 1] != s5.Seller.FullName)
                        s1.Add(s5.Seller.FullName);
                }
            }
        }
        return s1;
    }
}